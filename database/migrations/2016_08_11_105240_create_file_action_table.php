<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_action', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('file_id')->unsigned();
             $table->string('received_from');
             $table->string('action_to_be_done');
             $table->string('responsible_director');
             $table->string('comments');
            $table->timestamps();            
                 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_action');

    }
}
