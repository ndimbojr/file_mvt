<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('files', function (Blueprint $table){
             $table->increments('id');
             $table->integer('user_id');
             $table->string('file_reference_number');
             $table->date('date_created');
             $table->text('file_name');
             $table->text('index_heading');
             $table->date('closed_date')->nullable();
             $table->integer('status')->default(1);//1->active files, 2->closed files, 3->archived files.
             $table->timestamps();

         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
