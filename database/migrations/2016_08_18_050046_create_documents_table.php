<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id')->unsigned;
            $table->string('doc_folio_number');
            $table->string('doc_heading');
             $table->date('doc_written_date');
             $table->date('received_date');
            $table->string('category');
            $table->string('designation');
            $table->date('closed_date')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents');
    }
}
