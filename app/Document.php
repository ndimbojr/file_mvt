<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends LogModel
{
	public function files(){
		return $this->belongsTo('Document');
	}

     protected $fillable = [
          'file_id',
          'doc_folio_number',
     	'doc_heading',
     	'doc_written_date',
     	'received_date',
          'category',
     	'designation',
     	'closed_date'
     ];
}
