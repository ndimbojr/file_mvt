<?php

/*
handles user authentications 
sign up, login, reset password
*/

Route::get('auth/register', 'Auth\UsersController@getRegister');
Route::post('auth/register', 'Auth\UsersController@postRegister');

Route::get('auth/login', 'Auth\UsersController@getLogin');
Route::post('auth/login', 'Auth\UsersController@authenticate');
Route::get('auth/logout', 'Auth\UsersController@logout');

/*
handles methods like;
index,create, edit, show, store, update, destroy..  
*/

Route::group(['prefix' => 'files','middleware'=>'auth'], function(){

        Route::get('/', ['as'=>'files.index','uses'=>'FilesController@index', 'permission' => 'view_all_files']);
        
        Route::post('/', ['as'=>'files.create','uses'=>'FilesController@store', 'permission' => 'add_file']);

        Route::get('/create', ['as'=>'files.create','uses'=>'FilesController@create', 'permission' => 'add_file']);
		/*
		route to view director's requests
		*/
		Route::get('dir_request',['uses'=>'FilesController@dir_request','as'=>'files.dir_request.index', 'permission' => 'view_requests']);

        Route::get('/{id}', ['as'=>'files.update','uses'=>'FilesController@edit', 'permission' => 'edit_file']);

        Route::patch('/{id}', ['as'=>'files.edit','uses'=>'FilesController@update', 'permission' => 'edit_file']);



        Route::delete('/{id}', ['as'=>'files.destroy','uses'=>'FilesController@destroy', 'permission' => 'delete_file']);
        
    });

/*
 named route to create and store file requests sent to directors
*/

Route::post('files/filerequest',['uses'=>'FilesController@storefilerequest', 'as' =>'files.request.store', 'permission' =>  'request_action']);

Route::get('files/filerequest/{id}',['uses'=>'FilesController@filerequest', 'as' =>'files.filerequest.create', 'permission' =>  'request_action']);



/*
route to view all files of a authenticated director
*/

Route::get('files/dir_files',['uses'=>'FilesController@dir_files','as'=>'files.dir_files.index']);
/*
route to view all documents of a authenticated director
*/
Route::get('files/dir_documents',['uses'=>'FilesController@dir_documents','as'=>'files.dir_documents.index', 'permission' => 'view_particular_documents']);



/*
route to close a file display date.
*/

Route::delete('files/close_file/{file_id}',['uses'=>'FilesController@close_file','as'=>'files.close_file.index', 'permission' => 'close_file']);


/*
route to view document creation form
*/

Route::get('files/doc_create/{file_id}', ['uses'=>'FilesController@doc_create', 'as'=>'files.doc_create.create', 'permission'=> 'add_document']);
/*
route to store document info
*/
Route::post('files/doc_create',['uses'=>'FilesController@storedoc_create', 'as' =>'files.request.store', 'permission'=> 'add_document']);

// show list of documents

Route::get('files/documents/{file_id}',['uses'=>'FilesController@documents','as'=>'files.documents.index', 'permission' => 'view_particular_documents']);

// edit document

Route::get('files/documents/{id}/doc_edit', ['uses'=>'FilesController@doc_edit','as'=>'files.documents.doc_edit', 'permission'=> 'edit_document']);

Route::post('files/documents{id}', ['uses'=>'FilesController@doc_update','as'=>'files.documents.doc_update', 'permission'=> 'edit_document']);

// request for action
Route::get('files/doc_request/{id}',['uses'=>'FilesController@doc_request', 'as' =>'files.doc_request.create', 'permission' => 'request_action']);

Route::post('files/doc_request',['uses'=>'FilesController@storedoc_request', 'as' =>'files.doc_request.store', 'permission' => 'request_action']);

//close a document
Route::get('files/close_document/{id}',['uses'=>'FilesController@close_document','as'=>'files.close_document.index', 'permission' => 'close_document']);

// delete a document
Route::delete('files/documents/{id}', ['uses'=>'FilesController@doc_destroy', 'as'=>'documents.doc_destroy', 'permission' => 'delete_document']);

 // check if action is done
Route::delete('files/check_action/{id}',['uses'=>'FilesController@check_action','as'=>'files.check_action']);

//send notification
Route::get('emails/send_notification', ['uses'=>'FilesController@send_notification', 'as'=>'emails.send_notification']);


//  handles user roles
Route::resource('role','RoleController');

// add permissions
Route::get('role/permissions/{role_id}', ['uses'=>'RoleController@permissions', 'as'=>'role.permissions']);

Route::post('role/permissions/{role_id}', ['uses'=>'RoleController@store_permissions', 'as'=>'role.permissions.store']);

//assign role
Route::get('users',['uses'=>'RoleController@users','as'=>'role.users']);

Route::get('role/assign_role/{user_id}', ['uses'=>'RoleController@assign_role', 'as'=>'role.assign_role']);

Route::post('role/assign_role/{user_id}', ['uses'=>'RoleController@storeassign_role', 'as'=>'role.assign_role.store']);
