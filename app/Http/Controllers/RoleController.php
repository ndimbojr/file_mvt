<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Role;
use App\Permission;
use App\RolePermission;
use App\RoleUser;
use App\User;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of roles
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $roles = Role::All();

      return view('role.role',compact('roles'));
    }

    /**
     * Show the form for creating a new role
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view ('role/create_role');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $role= new Role ($request->all());

        $role->save();
        return redirect('role'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id); 

        return view('role.edit_role',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role= Role::findOrfail($id);

         $role->update($request->all());

        return redirect('role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $role = Role::find($id);

        $role->delete();

        return redirect('role');
    }

    public function permissions($role_id)
    {
         $role = Role::find($role_id);
          $role->perms;
          $permissions = Permission::All();

        return view('role.permissions',compact('permissions','role'));
    }

    public function store_permissions(Request $request, $role_id)
    {
        $role = Role::findOrFail($role_id);

        $role->perms()->sync($request->permission);

        return redirect('role');

    }

    // get all users from the database
    public function users()
    {
        $users = User::All();
      return view('role.users',compact('users'));  
    }

    //assign roles to users
    public function assign_role($user_id)
    {
          $user = User::findOrFail($user_id);
          $user->roles;
          $roles = Role::All();

        return view('role.assign_role',compact('roles', 'user'));
    }

    public function storeassign_role(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);

        $user->roles()->sync($request->role);

        return redirect('role');

    }
              
}
