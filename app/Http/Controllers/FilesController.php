<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FileRequest;
use App\Http\Requests\DocumentRequest;
use App\Http\Controllers\Controller;
use Redirect;
use App\Document;
use App\Document_action;
use App\File;
use App\File_action;
use App\Abbreviation;
use App\Directive;
use App\Notification;
use App\MailOut;
use App\User;
use Auth;
use Mail;



class FilesController extends Controller
{

       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('status')=='active'){
            $files=File::where('status','1')->get();
            
            return view('files.index',compact('files'));
        }
         else if($request->get('status')=='closed'){
            $files=File::where('status','2')->get();
            return view('files.index',compact('files'));
        } 
        else if($request->get('status')=='archived'){
            $files=File::where('status','3')->get();
            return view('files.index',compact('files'));
        }
        else{
            $files=File::All();
             return view('files.index',compact('files'));
        }
       

    }

       /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {

        $file= File::findOrfail($id);
        return view('files.show', compact('file'));
    }
       
   
      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view ('files.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(FileRequest $request)
    {
        $file = new File ($request->all());

        $file->save();
        
        return redirect('files'); 
    }

    
      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function edit($id)
    {
       $file= File::findOrfail($id);

        return view('files.edit', compact('file')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $file= File::findOrfail($id);

         $file->update($request->all());

        return redirect('files');
    }

    /**
     * Remove the file
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $file = File::find($id);

        $file->delete();

        return redirect('files');
    }


    public function filerequest($id)
    {  
        $file= File::findOrfail($id);
        
        $abbreviation = Abbreviation::lists('abbreviation','abbreviation');

        return view ('files.filerequest', compact('abbreviation'));
    }


    
    public function storefilerequest(Request $request)
    {      
        $data = $request->all();
        $data['received_from']=  Auth::user()->id;

        File_action::create($data);

       return redirect('files'); 
    }


    /**
     * Display a listing of director's requests
     *
     * @return \Illuminate\Http\Response
     */
    public function dir_request()
    {
       
        $dir_requests =  Auth::user()->requests;

        return view('files.dir_request',compact('dir_requests'));
    }



    public function dir_files()
    {
          $dir_files = File:: where('directorate', Auth::user()->directorate)->get();

        return view('files.dir_files',compact('dir_files'));  
    
    }

    // close a file
    public function close_file($id)
    {
        $file= File::find($id);


       $file->update(['closed_date'=>date('y-m-d'), 'status'=>2]);

        return redirect('files');
    }

    //view document form
    public function doc_create($file_id)
    {
        $file= File::findOrfail($file_id);

        $abbreviation = Abbreviation::lists('abbreviation','abbreviation');

        return view ('files.doc_create',compact('file_id', 'abbreviation'));
    }

    // store documents
    public function storedoc_create(DocumentRequest $request)
    {

         $document= new Document ($request->all());
        $document->save();
        $file_id=$document->file_id;
        $documents=Document::where('file_id',$file_id)->get();

        return redirect()->route('files.documents.index',$file_id);
    }

    // display list of documents

    public function documents($file_id)
    {

         $documents=Document::where('file_id',$file_id)->get();
        return view('files.documents',compact('documents','file_id'));
    }


    // display document form for editting
    public function doc_edit($id)
    {
       $document= Document::findOrfail($id);
       $abbreviation = Abbreviation::lists('abbreviation','abbreviation');
       
        return view('files.doc_edit', compact('document', 'abbreviation'))->with([ 'file_id'=> null]); 
    }

    // update documents
    public function doc_update(Request $request, $id)
    {    
         $document= Document::findOrfail($id);
         $document->update($request->all());
        return redirect()->route('files.documents.index',$document->file_id);
    }

    // request for action
    public function doc_request($id)
    {
        $document= Document::findOrfail($id);

        $directives = Directive::lists('directives','directives');
        $abbreviation = User::lists('abbreviation','id');

        return view('files.doc_request', compact('doc_request', 'abbreviation', 'document','directives'));
    }

    public function storedoc_request(Request $request)
    {
        $data = $request->all();

        $data['received_from']=  Auth::user()->id;
         
        $document_action = Document_action::create($data);

         $mailData = ['subject'=>'Request',
                     'body'=>'you have a request to attend',
                     'recipientMail'=>$document_action->responsible->email,
                     'recipientName'=>$document_action->responsible->fname, 
                     'cc'=>''
                     ];

         $mailOut = MailOut::create($mailData);

        return redirect('files');
    }
  

    //close document
    public function close_document($id)
    {
        $document= Document::find($id);

       $document->update(['closed_date'=>date('y-m-d')]);

        return redirect()->route('files.documents.index',$document->file_id);
    }

    // delete a document
    public function doc_destroy($id)
    {
        $document = Document::find($id);

        $document->delete();

        return redirect()->route('files.documents.index',$document->file_id);
    }

    // view all documents of an authenticated user 
    public function dir_documents()
    {
          $dir_documents = Document:: where('directorate', Auth::user()->role)->get();
        return view('files.dir_documents',compact('dir_documents')); 
    }

    // check if action is done
    public function check_action($id)
    {
        $dir_requests = Auth::user()->requests;
        $action = Document_action::findOrfail($id);
        $action->status = 1;
        $action->update();
        return view('files.dir_request', compact('dir_requests'));
    }

    // send notification
    public function send_notification()
    {
        Notification::send_notification();
    }
}