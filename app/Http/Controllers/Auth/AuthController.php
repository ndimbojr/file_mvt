<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $redirectPath = 'files';
   protected $redirectAfterLogout = 'auth/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
    * Show the application registration form.
    *
    * @return \Illuminate\Http\Response
    */
    public function getRegister()
   {
       return view('auth.register');
   }

    /**
    * Handle a registration request for the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function postRegister(Request $request)
    {
        {
           $validator = $this->validator($request->all());

           if ($validator->fails()) {
               $this->throwValidationException(
                   $request, $validator
               );
           }

            Auth::login($this->create($request->all()));

             return redirect()->intended('auth/login');
        }
    }

    /** Handle a login request to the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

     public function authenticate(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            $role = Auth::user()->role;
            if ($role=='entry'){
                return redirect()->intended('files');
            }
            if ($role=='director'){
                return redirect()->intended('files/dir_request');
            }
           
        }
    }
  /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => 'required|max:255',
            'mname' => 'required|max:255',
            'lname' => 'required|max:255',
            'position' => 'required|max:255',
            'abbreviation' => 'required|max:255',
            'extension_no' => 'required|max:255',
            'mobile_no' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        
        $password= $data['password'];

        $data['password'] = bcrypt($password);

         return User::create($data);


       /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),*/
     
    }

    public function logout()
    {
        Auth::logout();
        die ('execution failed');
       return view('auth.login'); 
        
    }
}
