<?php

namespace App\Http\Requests;

namespace App\Http\Requests;
use App\Http\Requests\Request;

class DocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doc_folio_number'=> 'required',
            'doc_heading'=> 'required',
            'doc_written_date'=> 'required',
            'received_date'=> 'required',
            'category'=> 'required',
            'designation'=> 'required',
             ];
    }
}
