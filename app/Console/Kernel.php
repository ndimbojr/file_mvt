<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Notification;
use App\Document_action;
use App\MailOut;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $notifications = Document_action::where( 'status', '0')
                        ->whereRaw('hour(timediff(now(), created_at)) = 24')->get();

        //echo $notifications->count();

        foreach ($notifications as $n) {
          // "Insert to mail out"
           // Notification::send_notification();
        }


        $mailouts = MailOut::where('status', 0)->get();

         foreach($mailouts as $mailout){
             $mailArray = $mailout->toArray();
             Notification::send_notification($mailArray);
             $mailout->status = 1;
             $mailout->save();


         }

    }
}
