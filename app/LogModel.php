<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Route;

class LogModel extends Model
{
 	

 	protected static function boot()
    {
        parent::boot();

        static::saving(function ($content) {
            $user = Auth::user();
            $id = $user->id;

            $action = explode("@", Route::currentRouteAction());
            $action = last($action);
            $logData= [
                        'user_id'=>$id,
                        'model'=>json_encode(get_class($content)),
                        'content'=>json_encode($content),
                        'action'=>$action,
                      ];
            $logData = Log::create($logData);
        });

    }

}


