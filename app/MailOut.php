<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailOut extends LogModel
{
    protected $table='mailOut';

    protected $fillable = [
    'subject',
    'body',
    'cc',
    'recipientMail',
    'recipientName',
    'status'
    ];
}
