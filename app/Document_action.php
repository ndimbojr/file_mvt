<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Route;

class Document_action extends LogModel
{
   protected $table='document_action';

    protected $fillable =[

       'doc_folio_number',
       'doc_heading',
       'category',
       'received_from',
       'action_to_be_done',
       'responsible_user',
       'status'
        ];


   public function request_from()
    {
        return $this->belongsTo('App\User', 'received_from');
    }

	 public function responsible()
    {
        return $this->belongsTo('App\User', 'responsible_user');
    }
}
