<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directive extends Model
{
    protected $fillable =[
    	'directives'
    ];
}
