 @extends('new_layout')
@section('content')
	<div id="viewfiles-wrap">
	<form action="{{route('role.assign_role',$user->id)}}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	 <div class="card-box table-responsive">
	 	<table>	
			<tbody>
		        @foreach($roles as $role)
					<tr>
			            <td>

			            <input type="checkbox" name="role[]" value="{{$role->id}}" 	{{($user->roles->contains('id',$role->id))?'checked="checked"':""}} 			            
			            />
							<!-- (condition)?statement1:statement2 ....ternary statement-->
			            
			            </td>
			            <td>{{$role->display_name}}</td>
			        </tr>
		        @endforeach
			      <!-- end foreach -->
			</tbody>
			</table>
    	</div>
    	
	    	 <button type="submit" class="btn btn-deep-green" id="btn-login" class="btn btn-primary form-control">Save</button>
    	 </form>
    </div>
@stop
