<!-- Form without header-->
<div id="doc_info">
    <div class="row">
    <div class="col-md-offset-2 col-md-8 col-md-offset-2">
        <div class="card">
            <div class="card-block">

                <!--Header-->
                <div class="text-xs-center">
                    <h3><i class="fa fa-pencil"></i> User Role:</h3>
                    <hr class="m-t-2 m-b-2">
                </div>

                <!--Body-->
                <br>

                <!--Body-->
                  <div class="form-group">
        
                        {!!Form::label('name','Name') !!}
                        {!!Form::text('name',null, ['class'=> 'form-control'])!!}
                        
                     </div>

                <div class="form-group">
                        
                        {!!Form::label('display_name','Display Name') !!}
                        {!!Form::text('display_name',null, ['class'=> 'form-control'])!!}
                        
                     </div>

                <div class="form-group">
                        
                        {!!Form::label('description','Description') !!}
                        {!!Form::text('description',null, ['class'=> 'form-control'])!!}
                        
                     </div>
                <!-- <div class="text-xs-center">
                    <button type="submit" id="btn-form" class="btn btn-deep-blue">Submit</button>
                </div> -->
                <div class="form-group">
                    <!-- <div class="submit-btn"> -->
                        {!!Form::submit($submitButtonText,['id'=>'submit-btn','class'=> 'btn btn-primary form-control'])!!}
                    <!-- </div> -->
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<!--/Form without header