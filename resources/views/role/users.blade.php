@extends('new_layout')

@section('content')
	<div class="x-panel">
		<div id="viewfiles-wrap">
			<div class="x-content">
				<div class="row">
					<div class="col-sm-12">
						<div class="card-box table-responsive">	
							<table id="table1" class="table table-striped table-bordered">
							<thead>
								<th>No.</th>
								<th>First Name</th>
								<th>Middle Name</th>
								<th>Last Name</th>
								<th>Position</th>
								<th>Abbreviation</th>
								<th>Mobile number</th>
								<th>Email</th>
								<th>Action</th>
							</thead>
							<tbody>
							@foreach($users as $user)
								<tr>
									<td>{{$user->id}}</td>
									<td>{{$user->fname}}</td>
									<td>{{$user->mname}}</td>
									<td>{{$user->lname}}</td>
									<td>{{$user->position}}</td>
									<td>{{$user->abbreviation}}</td>
									<td>{{$user->mobile_no}}</td>
									<td>{{$user->email}}</td>
									<td>
										<a class="btn btn-sm btn-default" href="{{route('role.assign_role', $user->id)}}"><i class="fa fa-cogs"></i>
					                </a>
									</td>
								</tr>
							@endforeach
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop