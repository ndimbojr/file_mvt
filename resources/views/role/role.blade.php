@extends('new_layout')

@section('content')

	<div id="viewfiles-wrap">
		<h3><i class="fa fa-user" aria-hidden="true"></i> Roles</h3>
		<hr>
		<a class="btn btn-default"  href="{{url('users')}}"><i class="fa fa-user"></i> Users</a>
		<div class="x_content">
			
                    <div class="card-box table-responsive">
						<table  id="table1" class="table table-hover table-bordered">	
						<thead class="thead">
							<th>No.</th>
								<th>Name</th>
								<th>Display Name</th>
								<th>Description</th>
								<th>Action</th>
						</thead>
						<tbody>
						@foreach($roles as $role)
							<tr>
								<td>{{$role->id}}</td>

								<td>{{$role->name}}</td>

								<td>{{$role->display_name}}</td>
								<td>{{$role->description}}</td>
								<td>
					                <a class="btn btn-sm btn-default" href="{{route('role.permissions', $role->id)}}"><i class="fa fa-cogs"></i>
					                </a>
					                <a class="btn btn-sm btn-default" href="{{route('role.edit',$role->id)}}"><i class="fa fa-pencil"></i>
					                </a>
					                <a class="btn btn-sm btn-default" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?" href="{{route('role.destroy', $role->id)}}"><i class="fa fa-trash"></i>
					                </a>
					            </td>
							</tr>
									
												
						@endforeach
						</tbody>

						</table>
					</div> <!-- end of card-box table-responsive --> 
			
		</div> <!-- end of x_content --> 
	</div> <!-- end of viewfiles-wrap -->

@stop
