@extends('new_layout')

@section('content')
	 {!!Form::model($role, array('route' => array('role.update', $role->id),'method'=>'PATCH')) !!}
	
	@include('role.role_form',['submitButtonText'=>'Save'])
	 
	 {!!Form::close() !!}

		@include('errors.list')

@stop