<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/mdb.min.css">
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{asset('css/new_app.css')}}">

	<link href="../js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="../images/logo.png">	
<title>FMS</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@yield('content')
			</div>
		</div>
	</div>
	<script src="../js/mdb.min.js"></script>	
	<script src="../js/jquery.min.js"></script>	
	<script src="../js/bootstrap.min.js"></script>	
	<script src="../js/datatables/jquery.dataTables.min.js"></script>	
	<script>
		$(document).ready(function(){
			$('#table1').DataTable();
		});
	</script>

	<script>
		// Window load event used just in case window height is dependant upon images
$(window).bind("load", function() { 
       
       var footerHeight = 0,
           footerTop = 0,
           $footer = $("#footer");
           
       positionFooter();
       
       function positionFooter() {
       
                footerHeight = $footer.height();
                footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
       
               if ( ($(document.body).height()+footerHeight) < $(window).height()) {
                   $footer.css({
                        position: "absolute"
                   }).animate({
                        top: footerTop
                   })
               } else {
                   $footer.css({
                        position: "static"
                   })
               }
               
       }

       $(window)
               .scroll(positionFooter)
               .resize(positionFooter)
               
});
	</script>

</body>
</html>