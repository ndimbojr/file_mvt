@extends('new_layout')

@section('content')
	<div class="x-panel">
		<div id="viewfiles-wrap">
			<h3><i class="fa fa-file-text" aria-hidden="true"></i> Documents</h3>
			<hr>
				<div class="x-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="card-box table-responsive">		
								<table id="table1" class="table table-striped table-bordered">
								<thead>
								<th>No.</th>
								<th>Folio/Minute no.</th>
								<th>Document written Date</th>
								<th>Received Date</th>
								<th>Category</th>
								<th>Responsible User</th>
								<th>Closed Date</th>
								<th>Action</th>
								</thead>
						<tbody>

						@foreach($dir_documents as $document)
						<tr>
							<td>{{$document->id}}</td>
							<td>{{$document->doc_folio_number}}</td>
							<td>{{$document->doc_written_date}}</td>
							<td>{{$document->received_date}}</td>
							<td>{{$document->category}}</td>
							<td>{{$document->designation}}</td>
							<td>{{$document->closed_date}}</td>
							<td>
								<a class="btn btn-sm btn-default" href="{{route('files.doc_request.create', $dir_request->id)}}"><i class="fa fa-comments"></i>
		                		</a>
		                	</td>
						<!-- <a href="{{route('files.filerequest.create', $file->id)}}">request</a> </td> -->
						</tr>
										
						@endforeach
						</tbody>
							</table>
						</div>
					</div>
				</div>
		</div>
	</div>
@stop