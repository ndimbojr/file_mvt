
<div id="form-wrap">
<h1>Request for Action</h1>
	<div class="form-group">
	 	
	 	{!!Form::label('action_needed','action to be taken') !!}
	 	{!!Form::textarea('action_to_be_done',null, [' class'=> 'form_control'])!!}
		
	 </div>

	 <div id="request-dropdown">
	 	<h3>Responsible Director</h3> <b>:</b>
					<select name="responsible_director">
					<option value="-"> </option>
					<option value="DCIO">DCIO</option>
					<option value="DBS">DBS</option>
					<option value="DESC">DESC</option>
					<option value="DIMS">DIMS</option>
					</select>

	 </div>
	 <div class="form-group">
	 	
		{!! Form::submit('request',['class'=> 'btn btn-primary form-control']) !!}
	 </div>

</div>