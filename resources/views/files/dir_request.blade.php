@extends('new_layout')

@section('content')
<div id="filerequests">
	<h3><i class="fa fa-file-text" aria-hidden="true"></i> Documents Requesting Actions</h3>
	<hr>
<!-- 	<a class="btn btn-default"  href="{{route('files.dir_documents.index')}}"><i class="fa fa-file left"></i> All Documents</a> -->
	<div class="x-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box table responsive">
					<table id="table1" class="table table-hover table-bordered">
						<thead>
							<th>No.</th>
							<th>Folio/Minute No.</th>
							<th>Document Heading</th>
							<th>Category</th>
							<th>Received from</th>
							<th>Action</th>
							<th>Action Requested on</th>
							<th>Add Action</th>
						</thead>
						<tbody>

					
					@foreach($dir_requests as $dir_request)

						<tr>
							<td>{{$dir_request->id}}</td>
							<td>{{$dir_request->doc_folio_number}}</td>
							<td>{{$dir_request->doc_heading}}</td>
							<td>{{$dir_request->category}}</td>
							<td>{{$dir_request->responsible->abbreviation or 'none'}}</td>
							<td>{{$dir_request->action_to_be_done}}</td>
							<td>{{$dir_request->created_at}}</td>
							<td>
								<a class="btn btn-sm btn-default" href="{{route('files.doc_request.create', $dir_request->id)}}"><i class="fa fa-comments"></i>
                				</a>
                				<a class="btn btn-sm btn-default" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?" title="check action" href="{{url('files/check_action?status=done', $dir_request->id)}}"><i class="fa fa-check"></i>
                				</a>
							</td>
						</tr>
					@endforeach
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@stop