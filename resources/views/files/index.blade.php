@extends('new_layout')

@section('content')

<div id="viewfiles-wrap">
	<ul class="nav nav-tabs nav-justified" role="tablist">
	    <li class="{{(Request::get('status')=='active')?'active':''}} tablist"><a href="{{url('files?status=active')}}">Active Files</a>
	    </li>
	    <li class=" {{(Request::get('status')=='closed')?'active':''}} tablist"><a href="{{url('files?status=closed')}}">Closed Files</a>
	    </li>
	    <li class=" {{(Request::get('status')=='archived')?'active':''}} tablist"><a href="{{url('files?status=archived')}}">Archived Files</a>
	    </li>
  	</ul>
		<h3><i class="fa fa-folder" aria-hidden="true"></i> Files</h3>
		<hr>
		
		<a class="btn btn-default" title="Add new File" href="{{route('files.create')}}"><i class="fa fa-plus left"></i> New</a>
		<a class="btn btn-default" title="Add new File" href="{{route('files.dir_request.index')}}"><i class="fa fa-comments left"></i> requests</a>
		<div class="x_content">
			
                    <div class="card-box table-responsive">
						<table  id="table1" class="table table-hover table-bordered">	
						<thead class="thead">
							<th>No.</th>
							<th width="3%">File Reference Number</th>
							<th>Date created</th>
							<th>File Name</th>
							<th>Index Heading</th>
							<th>Closed Date</th>
							<th>Action</th>
						</thead>
						<tbody>
						@foreach($files as $file)
							<tr>
								<td>{{$file->id}}</td>

								<td>{{$file->file_reference_number}}</td>

								<td>{{$file->date_created}}</td>
								<td>{{$file->file_name}}</td>
								<td>{{$file->index_heading}}</td>
								<td>{{$file->closed_date}}</td>
								<td>
					                <a class="btn btn-sm btn-default" title="Add new Document" href="{{route('files.doc_create.create', $file->id)}}"><i class="fa fa-plus"></i>
					                </a>
					                <a class="btn btn-sm btn-default" title="Edit File" href="{{route('files.edit', $file->id)}}"><i class="fa fa-pencil"></i>
					                </a>
					                <a class="btn btn-sm btn-default" title="Send Request" href="{{route('files.filerequest.create', $file->id)}}"><i class="fa fa-comments"></i>
					                </a>
					                <a class="btn btn-sm btn-default" title="View all Documents" href="{{route('files.documents.index', $file->id)}}"><i class="fa fa-files-o"></i>
					                </a>
					                <a class="btn btn-sm btn-default" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?" title="Close File" href="{{route('files.close_file.index', $file->id)}}"><i class="fa fa-times"></i>
					               </a>
					                <a class="btn btn-sm btn-default" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?" title="Delete File" href="{{route('files.destroy', $file->id)}}"><i class="fa fa-trash"></i>
					                </a>
					            </td>
							</tr>				
						@endforeach
						</tbody>

						</table>
					</div> <!-- end of card-box table-responsive --> 
			
		</div> <!-- end of x_content --> 
	</div> <!-- end of viewfiles-wrap -->

@stop
