@extends('new_layout')

@section('content')

<div id="filerequests">
<h1>files requesting actions</h1>

	<div class="view_files">
		<a href="{{route('files.dir_files.index')}}">view files</a>
	</div>	

	<table>
		<thead>
			<th>#</th>
			<th>file id</th>
			<th>received from</th>
			<th>action needed</th>
			<th>requested on</th>
			<th>add action</th>
		</thead>
		<tbody>

		@foreach($dir_requests as $dir_request)

		<tr>
				<td>{{$dir_request->i}}</td>
				<td>{{$dir_request->file_id}}</td>
				<td>{{$dir_request->getSender()}}</td>
				<td>{{$dir_request->action_to_be_done}}</td>
				<td>{{$dir_request->created_at}}</td>
				<td><a href="{{route('files.filerequest.create', $dir_request->id)}}">Add </a>
				</td>
			</tr>
			@endforeach
			</tbody>
	</table>

</div>

@stop