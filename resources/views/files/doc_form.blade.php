<!-- Form without header-->
<div id="doc_info">
    <div class="row">
    <div class="col-md-offset-2 col-md-8 col-md-offset-2">
        <div class="card">
            <div class="card-block">

                <!--Header-->
                <div class="text-xs-center form-heading">
                    <h3><i class="fa fa-pencil"></i> Document Information:</h3>
                    <hr class="m-t-2 m-b-2">
                </div>

                <!--Body-->
                <br>

                <!--Body-->
                        @if(!is_null($file_id))  
                        {!!Form::hidden('file_id',$file_id,['class'=> 'form-control'])!!}
                        @endif
                     <div class="form-group">
                        
                        {!!Form::label('doc_folio_number','Folio/minute number') !!}
                        {!!Form::text('doc_folio_number',null, ['class'=> 'form-control'])!!}
                        
                     </div>
                
                <div class="form-group">
                        
                        {!!Form::label('doc_heading','Ddocument Heading') !!}
                        {!!Form::text('doc_heading',null, [' class'=> 'form_control'])!!}
                        
                     </div>
                <div class="form-group">
                        
                        {!!Form::label('doc_written_date','Document written Date') !!}
                        {!!Form::date('doc_written_date',null, [' class'=> 'form_datetime'])!!}
                        
                     </div>

                <div class="form-group">
                        
                        {!!Form::label('received_date','Document received Date') !!}
                        {!!Form::date('received_date',null, [' class'=> 'form_datetime'])!!}
                        
                     </div>

                  <div id="request-dropdown">
                  <div class="chosen-position">
                  <select name="category" data-placeholder="Select Category" class="chosen-select chosen" style="width:200px;">
                  <option value="">Select Category </option>
                  <option value="minute_sheet">Minute Sheet</option>
                  <option value="loose_sheet">Loose Sheet</option>
                  <option value="Letter">Letter</option>
                  </select>
                  </div>
                </div>
                  
               <div id="request-dropdown">
                  <div class="chosen-position">
                  {!!Form::select('designation',$abbreviation,null,['class'=>'chosen-select chosen', 'style'=>'width:200px;'])!!}
                  </div>
                </div>
                <!-- <div class="text-xs-center">
                    <button type="submit" id="btn-form" class="btn btn-deep-blue">Submit</button>
                </div> -->
                <div class="form-group">
                    <!-- <div class="submit-btn"> -->
                        {!!Form::submit($submitButtonText,['id'=>'submit-btn','class'=> 'btn btn-primary form-control'])!!}
                    <!-- </div> -->
                 </div>

            </div>
        </div>
    </div>
</div>
</div>