<style type="text/css" media="screen">
.chosen-container-single .chosen-search input[type=text] {
    width: 90%;
    
}
.chosen-position{
  /* margin-left: 62px; */
}
</style>

<div id="doc_info">
	<div class="row">
    	<div class="col-md-offset-2 col-md-8 col-md-offset-2">
        	<div class="card">
            	<div class="card-block">
            		<!--Header-->
                <div class="text-xs-center form-heading">
                    <h3><i class="fa fa-comments"></i> Request for Action:</h3>
                    <hr class="m-t-2 m-b-2">
                </div>

                <br>
                <!--Body-->
                 <div class="form-group">
          	 	
        				 	{!!Form::label('doc_folio_number','Folio/Minute no.') !!}
        				 	{!!Form::text('doc_folio_number',null, [' class'=> 'form_control'])!!}
        					
        				 </div>
              
                <div class="form-group">
                  {!!Form::label('doc_heading','Document Heading') !!}
                  {!!Form::text('doc_heading',null, [' class'=> 'form_control'])!!}
                 </div>

                <!--  <div class="form-group">
                  {!!Form::label('action_needed','Action') !!}
                  {!!Form::textarea('action_to_be_done',null, [' class'=> 'form_control'])!!}
                 </div> -->

                 <div id="request-dropdown" style="width:200px;">
                  <div class="chosen-position">
                  {!!Form::select('action_to_be_done',$directives,null,['class'=>'chosen-select chosen', 'style'=>'width:200px;'])!!}
                  </div>
                </div>

                </div>
                <div id="request-dropdown">
                  <div class="chosen-position">
                  <select name="category" data-placeholder="Select Category" class="chosen-select chosen" style="width:200px;">
                  <option value="">Select Category </option>
                  <option value="minute_sheet">Minute Sheet</option>
                  <option value="loose_sheet">Loose Sheet</option>
                  <option value="Letter">Letter</option>
                  </select>
                  </div>
                </div>

        				<div id="request-dropdown">
                  <div class="chosen-position">
                  {!!Form::select('responsible_user',$abbreviation,null,['class'=>'chosen-select chosen', 'style'=>'width:200px;'])!!}
                  </div>
                </div>
                <div class="form-group">
      					{!! Form::submit('Request',['id'=>'submit-btn','class'=> 'btn btn-primary form-control']) !!}
      				 </div>
            	</div>
        	</div>
    	</div>
	</div>
</div>
          
    