@extends('new_layout')

@section('content')

	<div id="viewfiles-wrap">
		<h3><i class="fa fa-file-text" aria-hidden="true"></i> Documents</h3>
		<hr>
		<div class="createButton">
			<a class="btn btn-default" title="Add new Document" href="{{route('files.doc_create.create', $file_id)}}"><i class="fa fa-plus left"></i> New</a>
		</div>
		<div class="x_content">
			<div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
						<table  id="table1" class="table table-hover table-bordered">	
						<thead class="thead">
							<th>No.</th>
								<th>Folio/Minute no.</th>
								<th>Document Heading</th>
								<th>Document written Date</th>
								<th>Received Date</th>
								<th>Category</th>
								<th>Responsible User</th>
								<th>Closed Date</th>
								<th>Action</th>
						</thead>
						<tbody>
						@foreach($documents as $document)
							<tr>
								<td>{{$document->id}}</td>
								<td>{{$document->doc_folio_number}}</td>
								<td>{{$document->doc_heading}}</td>
								<td>{{$document->doc_written_date}}</td>
								<td>{{$document->received_date}}</td>
								<td>{{$document->category}}</td>
								<td>{{$document->designation}}</td>
								<td>{{$document->closed_date}}</td>
								<td>
					                <a class="btn btn-sm btn-default" title="Edit Document" href="{{route('files.documents.doc_edit', $document->id)}}"><i class="fa fa-pencil"></i>
					                </a>
					                <a class="btn btn-sm btn-default" title="Send Request" href="{{route('files.doc_request.create', $document->id)}}"><i class="fa fa-comments"></i>
					                </a>
					                <a class="btn btn-sm btn-default" title="Close Document" href="{{route('files.close_document.index', $document->id)}}"><i class="fa fa-times"></i>
					                </a>
					                <a class="btn btn-sm btn-default" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?" title="Delete Document" href="{{route('documents.doc_destroy', $document->id)}}"><i class="fa fa-trash"></i>
					                </a>

					            </td>
							</tr>
									
												
						@endforeach
						</tbody>

						</table>
					</div> <!-- end of card-box table-responsive --> 
				</div> <!-- end of col-sm-12 --> 
			</div> <!-- end of row -->  
		</div> <!-- end of x_content --> 
	</div> <!-- end of viewfiles-wrap -->

@stop
