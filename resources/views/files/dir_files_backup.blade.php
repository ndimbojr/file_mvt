@extends('new_layout')

@section('content')

<div class="x_panel">

<div id="viewfiles-wrap">
	<h1>files</h1>
	<div class="x_content">
		<div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">		
					<table id="table1" class="table table-striped table-bordered">	
						<thead>
							<th width="4%">No.</th>
								<th width="3%">file reference number</th>
								<th width="6%">date created</th>
								<th>file description</th>
								<th width="3%">letter reference number</th>
								<th width="3%">folio/minute number</th>
								<th>letter written date</th>
								<th width="6%">received date</th>
								<th>directorate</th>
								<th width="6%">closed date</th>
								<th width="13%">action</th>
						</thead>
						<tbody>

						@foreach($dir_files as $file)
						<tr>
						<td>{{$file->id}}</td>

						<td>{{$file->file_reference_number}}</td>

						<td>{{$file->date_created}}</td>
						<td>{{$file->file_description}}</td>
						<td>{{$file->letter_folio_number}}</td>
						<td>{{$file->letter_ref_number}}</td>
						<td>{{$file->letter_written_date}}</td>
						<td>{{$file->received_date}}</td>
						<td>{{$file->directorate}}</td>
						<td>{{$file->closed_date}}</td>
						<td><a href="{{route('files.filerequest.create', $file->id)}}">request</a> </td>
						</tr>
										
						@endforeach
						</tbody>
					</table>
					</div> <!-- end of card-box table-responsive --> 
				</div> <!-- end of col-sm-12 --> 
			</div> <!-- end of row -->  
		</div> <!-- end of x_content -->
	</div> <!-- end of viewfiles wrap -->
</div> <!-- end of x_panel -->
@stop
		


						
							

