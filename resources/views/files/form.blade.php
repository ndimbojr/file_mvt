<!-- Form without header-->
<div id="doc_info">
    <div class="row">
    <div class="col-md-offset-2 col-md-8 col-md-offset-2">
        <div class="card">
            <div class="card-block">

                <!--Header-->
                <div class="text-xs-center form-heading">
                    <h3><i class="fa fa-pencil"></i> File Information:</h3>
                    <hr class="m-t-2 m-b-2">
                </div>

                <!--Body-->
                <br>

                <!--Body-->
                  <div class="form-group">
        
                        {!!Form::label('file_reference_number','File Reference Number') !!}
                        {!!Form::text('file_reference_number',null, ['class'=> 'form-control'])!!}
                        
                     </div>

                <div class="form-group">
                        
                        {!!Form::label('date_created','Date created') !!}
                        {!!Form::date('date_created',null, ['class'=> 'form-control datepicker'])!!}
                        
                     </div>

                <div class="form-group">
                        
                        {!!Form::label('file_name','File Name') !!}
                        {!!Form::text('file_name',null, ['class'=> 'form-control'])!!}
                        
                     </div>

                <div class="form-group">
                        
                        {!!Form::label('index_heading','Index Heading') !!}
                        {!!Form::text('index_heading',null, ['class'=> 'form-control'])!!}
                        
                     </div>
                <!-- <div class="text-xs-center">
                    <button type="submit" id="btn-form" class="btn btn-deep-blue">Submit</button>
                </div> -->
                <div class="form-group">
                    <!-- <div class="submit-btn"> -->
                        {!!Form::submit($submitButtonText,['id'=>'submit-btn','class'=> 'btn btn-primary form-control'])!!}
                    <!-- </div> -->
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<!--/Form without header