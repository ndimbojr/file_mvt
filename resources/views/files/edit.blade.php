@extends('new_layout')

@section('content')
	 {!!Form::model($file, array('route' => array('files.update', $file->id),'method'=>'PATCH')) !!}
	
	@include('files.form',['submitButtonText'=>'Save'])
	 
	 {!!Form::close() !!}

		@include('errors.list')

@stop