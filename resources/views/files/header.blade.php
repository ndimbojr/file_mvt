
<nav class="navbar">
    <!-- <div class="container-fluid"> -->
    <div class="navbar-header">
        <a class="navbar-brand waves-effect waves-light" href="{{url('files')}}">File Movement System</a>
    </div>
            <!--Navbar icons-->
            <div class=" adminnav navbar-nav dropdown" class="dropdown-menu dropdown-primary" aria-labelledby="dropdownMenu1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
            <li class="" data-toggle="dropdown"><i class="fa fa-user"></i> {{Auth::user()->fname}}</li>
            <ul class="dropdown-menu">
                <li>
                    @if (Auth::user() != null)
                        <a href="{{url('auth/logout')}}">
                        <i class="fa fa-sign-out"></i> Logout</a> 
                    @endif
                </li>
                <li>
                    <a href="{{url('role')}}">
                    <i class="fa fa-user"></i> Admin
                </a>
                </li>
            </ul>
             </div>
        </div>
    </div>
</nav>



