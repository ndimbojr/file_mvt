@extends('app')

@section('content')
<!--Form with header-->
    <div class="row">
        <div class="col-md-offset- 3 col-md-6 col-md-offset-3">
    <div class="card z-depth-3">
        <div class="card-block">
        <form class="form-horizontal login" role="form" method="POST" action="{{ url('/auth/login') }}">
        <!--Header-->
        <div class="form-header  teal darken-3">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h3><i class="fa fa-lock"></i> Welcome to FMS</h3>
        </div>

        <!-- handles errors if any -->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

        <!--Body-->
        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="text"  class="form-control" placeholder="Your email" name="email" value="{{ old('email') }}">
            <!-- <label for="form2">Your email</label> -->
        </div>

        <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input type="password" id="form4" class="form-control" placeholder="Password" name="password" >
            <!-- <label for="form4">Your password</label> -->
        </div>

        <div class="text-xs-center">
            <button type="submit" class="btn btn-deep-green" id="btn-login">Login</button>
        </div>
    </form>
    </div>

    <!--Footer-->
    <div class="modal-footer">
        <div class="options">
            <p>Not a member? <a href="{{url('/auth/register')}}">Sign Up</a></p>
            <p>Forgot <a href="{{ url('emails/password') }}">Password?</a></p>
        </div>
    </div>

</div>
        </div>
    </div>
<!--/Form with header-->
@endsection